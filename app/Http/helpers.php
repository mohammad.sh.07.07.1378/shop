<?php

use Illuminate\Support\Facades\Route;

if (!function_exists('isActive')) {
    function isActive($key, $name = 'active')
    {
        if (is_array($key)) {
            return in_array(Route::currentRouteName(), $key) ? $name : '';
        }
        return Route::currentRouteName() == $key ? $name : '';
    }
}
if (!function_exists('isUrl')) {
    function isUrl($url, $name = 'active')
    {
        return request()->fullUrlIs($url) ? $name : '';
    }
}
