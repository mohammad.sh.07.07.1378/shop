<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use Modules\Product\Entities\Product;

class IndexController extends Controller
{
    public function index()
    {
        $products = Product::query();
        if ($name = \request('search')) {
            $products->where('name', 'LIKE', "%{$name}%");
        }
        if ($cat = \request('cate')) {
            dd(\request('cate'));
        }
        $products = $products->latest()->paginate(5);

        return view('index.index', compact('products'));
    }
}
