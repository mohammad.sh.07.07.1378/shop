<ul>
    @foreach($category as $lvl)
        <li>
            <a href="#">
                {{$lvl->name}}
            </a>
            @if($lvl->child->count())
                @include('layout.category',['category'=>$lvl->child])
            @endif
        </li>
    @endforeach
</ul>

