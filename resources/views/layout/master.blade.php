<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Easy Shop</title>
    <link href="/style/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="/style/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/style/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="/style/owl.theme.default.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css"/>
    <link href="/style/style.css" rel="stylesheet" type="text/css">
    <script src="{{ asset('js/app.js') }}"></script>
</head>
<body>
@include('sweet::alert')
<div class="social">
    <ul>
        <li><a href=""><i class="fa fa-instagram"></i></a></li>
        <li><a href=""><i class="fa fa-send"></i></a></li>
        <li><a href=""><i class="fa fa-facebook"></i></a></li>
        <li><a href=""><i class="fa fa-twitter"></i></a></li>
    </ul>
</div>
<!---------------------------------->
<div class="top2">
    <div class="container">
        <div class="row">
            <div class="col-md-6 flex-column">
                <div class="login">
                    @if(auth()->check())
                        کاربر
                        <span style="color: #0b5ed7">{{auth()->user()->name}}</span>
                        خوش آمدید!
                        <a href="{{route('dashboard')}}" class="mybtn"><i class="fa fa-dashboard"></i>داشبورد</a>

                    @else
                        <a href="{{route('register')}}" class="mybtn"><i class="fa fa-user-plus"></i>ثبت نام</a>
                        <a href="{{route('login')}}" class="mybtn"><i class="fa fa-user-o"></i>ورود</a>
                    @endif
                    @if(!isActive('cart'))
                        <a href="{{route('cart')}}" class="mybtn"><i class="fa fa-cart-arrow-down"></i>سبد</a>
                    @else
                        <a href="{{route('index')}}" class="mybtn"><i class="fa fa-cart-arrow-down"></i>بازگشت ب محصولات</a>

                    @endif
                    @if(auth()->check())
                        <form action="{{route('logout')}}" method="post" id="logout">
                            @csrf
                        </form>
                        <a class="mybtn" href=""
                           onclick="event.preventDefault();document.getElementById('logout').submit()">خروج<i
                                class="fa fa-sign-out"></i></a>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <form action="">
                    <input type="text" placeholder="کالای مورد نظر را جستجو کنید">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
</div><!--top2-->
<!---------------------------------->
@php
    $category=\Modules\Category\Entities\Category::all();
    $level1=$category->where('parent',0);
@endphp
<div class="main-menu">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('layout.category',['category'=>$level1])
            </div>
        </div>
    </div>
</div>

@yield('main')

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="footer-description">
                    <ul>
                        <li>تضمین اصالت کالاهای فروخته شده</li>
                        <li>فروش برند های معتبر</li>
                        <li>پاسخگویی 24 ساعته</li>
                        <li>امکان پرداخت آنلاین با کارت بانکی و پرداخت در محل</li>
                        <li>امکان بازگشت تا یک هفته در صورت عدم رضایت مشتری</li>
                        <li>خرید آسان و مطمئن</li>
                        <li>قیمت های مناسب</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="footer-description2">
                    <ul>
                        <li><i class="fa fa-truck"></i>تحویل پستی سریع</li>
                        <li><i class="fa fa-plane"></i>ارسال با پست پیشتاز و سفارشی</li>
                        <li><i class="fa fa-cart-arrow-down"></i>خرید آسان و راحت</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="news-form">
                    <h5>در خبرنامه عضو شوید</h5>
                    <form action="">
                        <input type="email" placeholder="ایمیل خود را وارد کنید">
                        <button type="submit"><i class="fa fa-envelope-o"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!---------------------------------->
<div class="copy-right">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                &copy;&nbsp;&nbsp;طراحی و کدنویسی سئو 90&nbsp;&nbsp;&nbsp;&nbsp;
                <span>info@seo90.ir</span>
            </div>
        </div>
    </div>
</div>
<!---------------------------------->
<script src="/js/jquery-3.3.1.js" type="text/javascript"></script>
<script src="/js/jquery.simple.timer.js" type="text/javascript"></script>
<script src="/js/bootstrap.js" type="text/javascript"></script>
<script src="/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="/js/js.js" type="text/javascript"></script>
@yield('script')
</body>
</html>
