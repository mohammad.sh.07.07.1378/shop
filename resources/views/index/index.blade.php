@extends('layout.master')


@section('main')
    <div class="pages-bnaer text-center">
        <div class="container">
            <span>محصولات</span>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @foreach($products as $product)
                <div class="col-md-4">
                    <div class="blog-content">
                        <figure>
                            <img src="{{$product->image}}" class="w-100">
                        </figure>
                        <h5><i class="fa fa-title"></i>{{$product->title}}</h5>
                        <p>{{$product->description}}</p>
                        <ul>
                            <li><i class="fa fa-bars"></i>دسته بندی : تکنولوژی</li>
                            <li><i class="fa fa-calendar-o"></i>نوشته شده در : {{jdate($product->create_at)}}</li>
                        </ul>
                        <a href="{{route('single.product',$product->id)}}" class="mybtn"><i
                                class="fa fa-continuous"></i>ادامه مطلب&raquo;</a>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="mb-2 mt-2 align-content-center">
            {{$products->render()}}
        </div>
    </div>
@endsection
