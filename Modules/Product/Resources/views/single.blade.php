@extends('layout.master')
@section('script')
    <script>
        $('#sendComment').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)// Button that triggered the modal
            let parent_id = button.data('id');
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('input[name="parent_id"]').val(parent_id)
        })
    </script>
@endsection
@section('main')

    @auth
        <div class="modal fade" id="sendComment">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">ارسال نظر</h5>
                        <button type="button" class="close mr-auto ml-0" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form action="{{ route('send.comment') }}" method="post" id="sendCommentForm">
                        @csrf
                        <div class="modal-body">
                            <input type="hidden" name="commentable_id" value="{{ $product->id }}">
                            <input type="hidden" name="commentable_type" value="{{ get_class($product) }}">
                            <input type="hidden" name="parent_id" value="0">

                            <div class="form-group">
                                <label for="message-text" class="col-form-label">پیام دیدگاه:</label>
                                <textarea name="comment" class="form-control" id="message-text"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">لغو</button>
                            <button type="submit" class="btn btn-primary">ارسال نظر</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endauth

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="single-box">
                    <div class="row">
                        <div class="col-md-7">
                            <h5>{{$product->title}}</h5>
                            {{--                            <h6>تبلت هوآوی 10 اینچ فورجی</h6>--}}
                            <hr>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="single-content-right">
                                        <ul class="brand-ul">
                                            <li>برند : <a href="#">هوآوی</a></li>
                                            <li>دسته بندی : <a href="#">موبایل و تبلت</a></li>
                                        </ul>
                                        <br>
                                        <span>مشخصات مختصر محصول :</span><br>
                                        <ul class="product-ul">
                                            <li></li>
                                            <li>اندازه نمایشگر 10 اینچ</li>
                                            <li>دارای شبکه 4G</li>
                                            <li>دارای دو سیم کارت</li>
                                            <li>حافظه داخلی 32G</li>
                                            <li>دارای بدنه فلزی در عین حال سبک</li>
                                            <li>درای تنوع رنگ</li>
                                            <li>توضیحات :{{$product->description}}</li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="single-content-left">
                                        <ul>
                                            <span>وضعیت :
                                                @if($product->inventory)
                                                    موجود در انبار
                                                @else
                                                    موجود نیست
                                                @endif
                                            </span><br><br>
                                            <li>گارانتی : Huawei</li>
                                            <br>
                                            <li>
                                                رنگ بندی :
                                                <ul>
                                                    <li><i class="fa fa-square white"></i>سفید</li>
                                                    <li><i class="fa fa-square black"></i>مشکی</li>
                                                    <li><i class="fa fa-square silver"></i>نقره ای</li>
                                                    <li><i class="fa fa-square gold"></i>طلایی</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <hr>

                            @if ((\Modules\Cart\Helper\Cart::count($product) < $product->inventory))
                                <h3>{{$product->price}} تومان</h3>

                                <div class="btn-single">
                                    <form action="{{route('add.cart',$product->id)}}" method="post" id="add_to_cart">
                                        @csrf
                                    </form>
                                    <span style="cursor: pointer"
                                          onclick="document.getElementById('add_to_cart').submit()">
                                    <i class="fa fa-cart-plus"></i>
                                    اضاف به سبد
                                    </span>
                                </div>

                            @else

                                <div class="btn-single">

                                <span>
                                    <i class="fa fa-hourglass-end color-theme-1"></i>موجود نیست</span>
                                </div>


                            @endif
                        </div>

                        <div class="col-md-5">
                            <div class="single-img">
                                <figure>
                                    <img src="/img/single-tablet.jpg" class="w-100 s-img"
                                         data-zoom-image="img/single-tablet.jpg">
                                </figure>
                            </div>
                            <div class="single-img-slider">
                                <div class="owl-carousel owl-theme ov-single">
                                    <div class="item">
                                        <a data-fancybox="gallery" href="/img/single-tablet.jpg"><img
                                                src="/img/single-tablet.jpg" class="w-100"></a>
                                    </div>
                                    <div class="item">
                                        <a data-fancybox="gallery" href="/img/single-tablet.jpg"><img
                                                src="/img/single-tablet.jpg" class="w-100"></a>
                                    </div>
                                    <div class="item">
                                        <a data-fancybox="gallery" href="/img/single-tablet.jpg"><img
                                                src="/img/single-tablet.jpg" class="w-100"></a>
                                    </div>
                                    <div class="item">
                                        <a data-fancybox="gallery" href="/img/single-tablet.jpg"><img
                                                src="/img/single-tablet.jpg" class="w-100"></a>
                                    </div>
                                    <div class="item">
                                        <a data-fancybox="gallery" href="/img/single-tablet.jpg"><img
                                                src="/img/single-tablet.jpg" class="w-100"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-2 mb-2">
            <div class="col">
                <div class="d-flex align-items-center justify-content-between">
                    <h4 class="mt-4">بخش نظرات</h4>
                    @auth
                        <span class="btn btn-sm btn-primary" data-toggle="modal" data-target="#sendComment" data-id="0">ثبت نظر جدید</span>
                    @endauth
                </div>

                @guest
                    <div class="alert alert-warning">برای ثبت نظر لطفا وارد سایت شوید.</div>
                @endguest

                @include('comment::comments',['comments'=>$product->comments()->where('parent_id' , 0)->where('approved',1)->get()])
            </div>
        </div>
    </div>
@endsection
