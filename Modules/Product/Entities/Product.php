<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Attribute\Entities\Attribute;
use Modules\Attribute\Entities\AttributeProductValue;
use Modules\Category\Entities\Category;
use Modules\Comment\Entities\Comment;
use Modules\Discount\Entities\Discount;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'price',
        'image',
        'inventory',
        'view_count'
    ];

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function discounts()
    {
        return $this->hasMany(Discount::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class)->using(AttributeProductValue::class)->withPivot(['value_id']);
    }

}
