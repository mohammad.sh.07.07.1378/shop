<?php

namespace Modules\Attribute\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AttributeValue extends Model
{
    protected $fillable = ['value'];

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class);
    }
}
