<?php

namespace Modules\Discount\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cart\Helper\Cart;
use Modules\Discount\Entities\Discount;

class DiscountController extends Controller
{
    public function cart(Request $request)
    {

        $data = $request->validate([
            'discount' => ['required', 'exists:discounts,code']
        ]);


        if (!auth()->check()) {
            return back()->withErrors([
                'code' => 'برای اعمال کد تخفیف باید وارد سایت بشین'
            ]);
        }

        $code = Discount::where('code', $data['discount'])->first();


        if ($code->expired_at < now()) {
            return back()->withErrors([
                'code' => 'مهلت استفاده از کد گذشته است'
            ]);
        }

        if ($code->users()->count()) {
            if (!in_array(auth()->user()->id, $code->users()->plunk('id')->toArray())) {
                return back()->withErrors([
                    'code' => 'شما قادر به استفاده از این کد نیستین'
                ]);
            }
        }

        Cart::addDiscount($data['discount']);

        return back();

    }

    public function delete()
    {
        Cart::addDiscount(null);
        return back();
    }
}
