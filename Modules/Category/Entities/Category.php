<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Discount\Entities\Discount;
use Modules\Product\Entities\Product;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'parent'];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function child()
    {
        return $this->hasMany(Category::class, 'parent', 'id');
    }
    public function discounts()
    {
        return $this->hasMany(Discount::class);
    }
}
