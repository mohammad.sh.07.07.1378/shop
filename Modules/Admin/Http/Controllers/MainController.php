<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;


class MainController extends Controller
{

    public function index()
    {
        $modules = \Module::all();
        return view('admin::admin.module.module', compact('modules'));
    }

    public function disable($module)
    {
        $module = \Module::find($module);

        if (\Module::canDisable($module))
            $module->disable();

        return back();
    }

    public function enable($module)
    {

        $module = \Module::find($module);

        if (\Module::canDisable($module))
            $module->enable();

        return back();
    }
}
