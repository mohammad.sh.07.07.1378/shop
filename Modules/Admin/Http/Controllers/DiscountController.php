<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Discount\Entities\Discount;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $discounts = Discount::latest()->paginate(30);
        return view('admin::admin.discount.all', compact('discounts'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('admin::admin.discount.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'code' => 'required|unique:discounts,code',
            'percent' => 'required|integer|between:1,99',
            'expired_at' => 'required',
            'products' => 'exists:products,id|array|nullable',
            'categories' => 'exists:categories,id|array|nullable',
            'users' => 'exists:users,id|array|nullable'
        ]);


        $discount = Discount::create($data);
        if (isset($data['products'])) {
            $discount->products()->attach($data['products']);
        }
        if (isset($data['categorise'])) {
            $discount->categorise()->attach($data['categorise']);
        }
        if ($data['users']) {
            $discount->users()->attach($data['users']);
        }


        alert()->success('ذخیره شده');

        return redirect(route('admin.discount.index'));

    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Discount $discount)
    {
        return view('admin::admin.discount.edit', compact('discount'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, Discount $discount)
    {
        $data = $request->validate([
            'code' => ['required', Rule::unique('discounts', 'code')->ignore($discount->id)],
            'percent' => 'required|integer|between:1,99',
            'expired_at' => 'required',
            'products' => 'exists:products,id|array|nullable',
            'categories' => 'exists:categories,id|array|nullable',
            'users' => 'exists:users,id|array|nullable'
        ]);

        $discount->update($data);
        isset($data['products'])
            ? $discount->products()->sync($data['products'])
            : $discount->products()->detach();
        isset($data['categories'])
            ? $discount->categories()->attach($data['categories'])
            : $discount->categories()->detach();
        isset($data['users'])
            ? $discount->users()->attach($data['users'])
            : $discount->users()->detach();

        alert()->success('ذخیره شده');

        return redirect(route('admin.discount.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Discount $discount)
    {
        $discount->delete();
        alert()->success('حذف شده');
        return redirect(route('admin.discount.index'));
    }
}
