<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Comment\Entities\Comment;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $comments = Comment::query();

        if ($keyboard = \request('search')) {
            $comments->where('comment', 'LIKE', "%{$keyboard}%")
                ->orwhereHas('user', function ($query) use ($keyboard) {
                    $query->where('name', 'LIKE', "%{$keyboard}%");
                });
        }

        $comments = $comments->whereApproved(1)->latest()->paginate(20);
        return view('admin::admin.comments.all', compact('comments'));
    }

    public function unapproved()
    {
        $comments = Comment::query();

        if ($keyboard = \request('search')) {
            $comments->where('comment', 'LIKE', "%{$keyboard}%")
                ->orwhereHas('user', function ($query) use ($keyboard) {
                    $query->where('name', 'LIKE', "%{$keyboard}%");
                });
        }
        $comments = $comments->whereApproved(0)->latest()->paginate(20);
        return view('admin::admin.comments.unapproved', compact('comments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {

        $comment->update([
            'approved' => true
        ]);

        alert()->success('تایید شد');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        alert()->success('حذف شد');
        return back();
    }
}
