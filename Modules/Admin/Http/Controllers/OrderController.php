<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Order\Entities\Order;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::query();

        if ($search = \request('search')) {
            $orders->where('id', $search)->orWhere('tracing_serial', $search);
        }
        $orders = $orders->where('status', \request('type'))->latest()->paginate(15);

        return view('admin::admin.orders.all', compact('orders'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function payment(Order $order)
    {
        $payments = $order->payments()->latest()->paginate(10);
        return view('admin::admin.orders.payments', compact('payments','order'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {

        $this->authorize('view', $order);
        $products = $order->products()->latest()->paginate(10);
        return view('admin::admin.orders.details', compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('admin::admin.orders.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $data = $this->validate($request, [
            'status' => ['required', Rule::in(['unpaid', 'paid', 'preparation', 'posted', 'received', 'canceled'])],
            'tracing_serial' => 'required'
        ]);
        $order->update($data);
        alert()->success('موفقیت آمیز بود');

        return redirect(route('admin.orders.index') . "?type=$order->status");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();

        alert()->success('حذف شد');

        return back();
    }
}
