@component('admin::admin.layout.content',['title'=>'مدیریت محصولات'])

    @slot('breadcrumb')
        <li class="breadcrumb-item"><a href="/admin">پنل محصولات</a></li>
        <li class="breadcrumb-item active">محصولات</li>
    @endslot
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"> محصولات</h3>

                    <div class="card-tools d-flex float-left">
                        <div class="input-group input-group-sm d-flex" style="width: 200px;">
                            <form action="" class="d-flex">
                                <input type="text" name="search" class="form-control float-right"
                                       placeholder="جستجو">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>

                        <div class="float-left  mr-2">
                            @can('create-product')
                                <a class="btn btn-sm btn-info" href="{{route('admin.product.create')}}">ایجاد محصول</a>
                            @endcan
                        </div>
                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>آیدی</th>
                            <th>نام</th>
                            <th>قیمت</th>
                            <th>موجودی</th>
                            <th>عملیات</th>
                        </tr>
                        @foreach($products as $product)
                            <tr>
                                <td>{{$product->id}}</td>
                                <td>{{$product->title}}</td>
                                <td>{{$product->price}}</td>
                                <td>{{$product->inventory}}</td>
                                <td class="d-flex btn-sm">
                                    @can('delete-product')
                                        <form action="{{route('admin.product.destroy',['product'=>$product->id])}}"
                                              method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger ml-2">حذف</button>
                                        </form>
                                    @endcan
                                    @can('edit-product')
                                        <a href="{{ route('admin.product.edit',['product'=>$product->id]) }}"
                                           class="btn btn-sm btn-primary">ویرایش</a>
                                    @endcan
                                    @can('product-gallery')
                                        <a href="{{ route('admin.product.gallery.index',['product'=>$product->id]) }}"
                                           class="btn btn-sm mr-2 btn-warning">گالری تصاویر</a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{$products->render()}}
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>


@endcomponent
