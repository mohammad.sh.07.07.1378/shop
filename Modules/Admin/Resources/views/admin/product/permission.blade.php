@component('admin::admin.layout.content',['title'=>'دسترسی ها'])

    @slot('breadcrumb')
        <li class="breadcrumb-item"><a href="">پنل دسترسی ها</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">کاربران</a></li>
        <li class="breadcrumb-item active">ایجاد دسترسی ها</li>
    @endslot

    @slot('script')
        <script>
            $('#permission').select2({
                'placeholder':'دسترسی را انتخاب کنید'
            });
            $('#role').select2({
                'placeholder':'مقام را انتخاب کنید',
            });

        </script>
    @endslot
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">دسترسی</div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $err)
                                    <li>
                                        {{ $err }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="{{route('admin.user.permission.store',$user->id)}}" class="p-3">
                        @csrf
                        <div class="form-group row">

                            <label for="permission"
                                   class="col-md-4 col-form-label text-md-right">مقام ها</label>
                            <select name="role[]" id="role" class="form-control" multiple>
                                @foreach(\App\Role::all() as $role)

                                    <option
                                        value="{{$role->id}}" {{in_array($role->id,$user->roles->pluck('id')->toArray()) ? 'selected' : ''}}>{{$role->name}}</option>

                                @endforeach
                            </select>
                        </div>
                        <div class="form-group row">
                            <label for="permission"
                                   class="col-md-4 col-form-label text-md-right">دسترسی ها(زیر مجموعه) : </label>
                            <select name="permission[]" id="permission" class="form-control" multiple>
                                @foreach(\App\Permission::all() as $permission)

                                    <option
                                        value="{{$permission->id}}" {{in_array($permission->id,$user->permissions->pluck('id')->toArray()) ? 'selected' : ''}}>{{$permission->name}}</option>

                                @endforeach
                            </select>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    ثبت دسترسی
                                </button>
                                <a href="{{ route('admin.users.index') }}" class="btn btn-default mr-3">
                                    انصراف
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endcomponent
