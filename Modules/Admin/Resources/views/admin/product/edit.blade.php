@component('admin::admin.layout.content',['title'=>'ایجاد محصول'])

    @slot('breadcrumb')
        <li class="breadcrumb-item"><a href="/admin">پنل مدیریت</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.product.index') }}">محصول</a></li>
        <li class="breadcrumb-item active">ویرایش محصول</li>
    @endslot
    @slot('script')
        <script>


            //file manger
            document.addEventListener("DOMContentLoaded", function () {

                document.getElementById('button-image').addEventListener('click', (event) => {
                    event.preventDefault();

                    window.open('/file-manager/fm-button', 'fm', 'width=1400,height=800');
                });
            });

            // set file link
            function fmSetLink($url) {
                document.getElementById('image_label').value = $url;
            }

            //end file manger


            $('#categories').select2({
                'placeholder': 'دسترسی مورد نظر را انتخاب کنید'
            })


            let changeAttributeValues = (event, id) => {
                let valueBox = $(`select[name='attributes[${id}][value]']`);


                //
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content,
                        'Content-Type': 'application/json'
                    }
                })
                //
                $.ajax({
                    type: 'POST',
                    url: '/admin/attribute/values',
                    data: JSON.stringify({
                        name: event.target.value
                    }),
                    success: function (res) {
                        valueBox.html(`
                            <option value="" selected>انتخاب کنید</option>
                            ${
                            res.data.map(function (item) {
                                return `<option value="${item}">${item}</option>`
                            })
                        }
                        `);
                    }
                });
            }

            let createNewAttr = ({attributes, id}) => {

                return `
                    <div class="row" id="attribute-${id}">
                        <div class="col-5">
                            <div class="form-group">
                                 <label>عنوان ویژگی</label>
                                 <select name="attributes[${id}][name]" onchange="changeAttributeValues(event, ${id});" class="attribute-select form-control">
                                    <option value="">انتخاب کنید</option>
                                    ${
                    attributes.map(function (item) {
                        return `<option value="${item}">${item}</option>`
                    })
                }
                                 </select>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="form-group">
                                 <label>مقدار ویژگی</label>
                                 <select name="attributes[${id}][value]" class="attribute-select form-control">
                                        <option value="">انتخاب کنید</option>
                                 </select>
                            </div>
                        </div>
                         <div class="col-2">
                            <label >اقدامات</label>
                            <div>
                                <button type="button" class="btn btn-sm btn-warning" onclick="document.getElementById('attribute-${id}').remove()">حذف</button>
                            </div>
                        </div>
                    </div>
                `
            }

            $('#add_product_attribute').click(function () {
                let attributesSection = $('#attribute_section');
                let id = attributesSection.children().length;

                let attributes = $('#attributes').data('attributes');

                attributesSection.append(
                    createNewAttr({
                        attributes,
                        id
                    })
                );

                $('.attribute-select').select2({tags: true});
            });

            $('.attribute-select').select2({tags: true});
        </script>
    @endslot
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">ویرایش محصول</div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $err)
                                    <li>
                                        {{ $err }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div id="attributes"
                         data-attributes="{{ json_encode(\Modules\Attribute\Entities\Attribute::all()->pluck('name')) }}"></div>
                    <form method="POST" action="{{route('admin.product.update',$product->id)}}" class="p-3"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">نام محصول :</label>

                            <div class="col-md-6">
                                <input id="title" type="text"
                                       class="form-control @error('title') is-invalid @enderror" name="title"
                                       value="{{ $product->title }}" required autocomplete="title" autofocus>

                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>

                        <div class="form-group row">
                            <label for="price"
                                   class="col-md-4 col-form-label text-md-right">قیمت : </label>

                            <div class="col-md-6">
                                <input id="email" type="number"
                                       class="form-control @error('price') is-invalid @enderror" name="price"
                                       value="{{ $product->price }}" required autocomplete="price">

                                @error('price')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label for="inventory"
                                   class="col-md-4 col-form-label text-md-right">موجودی : </label>

                            <div class="col-md-6">
                                <input id="inventory" type="number"
                                       class="form-control @error('inventory') is-invalid @enderror" name="inventory"
                                       required autocomplete="new-inventory" value="{{$product->inventory}}">

                                @error('inventory')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="image" class="col-sm-2 control-label">عکس محصول</label>


                            <div class="input-group">
                                <input type="text" id="image_label" class="form-control" name="image"
                                       aria-label="Image" aria-describedby="button-image" value="{{$product->image}}">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="button-image">Select
                                    </button>
                                </div>
                            </div>

                            <img class="w-25 mt-2" src="{{ $product->image }}" alt="">
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label for="description"
                                   class="col-md-4 col-form-label text-md-right">توضیحات : </label>

                            <div class="col-md-6">
                                <textarea id="description" style="width:600px;height:250px" class="form-control"
                                          name="description" required
                                          autocomplete="description">{{$product->description}}
                                </textarea>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label for="permission"
                                   class="col-md-4 col-form-label text-md-right">دسته بندی ها : </label>
                            <select name="categories[]" id="categories" class="form-control" multiple>
                                @foreach(\Modules\Category\Entities\Category::all() as $category)

                                    <option
                                        value="{{$category->id}}" {{in_array($category->id,$product->categories->pluck('id')->toArray())?'selected':''}}>{{$category->name}}</option>

                                @endforeach
                            </select>
                        </div>
                        <hr>
                        <h6>ویژگی محصول</h6>
                        <hr>
                        <div id="attribute_section">
                            @foreach($product->attributes as $attribute)
                                <div class="row" id="attribute-{{ $loop->index }}">
                                    <div class="col-5">
                                        <div class="form-group">
                                            <label>عنوان ویژگی</label>
                                            <select name="attributes[{{ $loop->index }}][name]"
                                                    onchange="changeAttributeValues(event, {{ $loop->index }});"
                                                    class="attribute-select form-control">
                                                <option value="">انتخاب کنید</option>
                                                @foreach(\Modules\Attribute\Entities\Attribute::all() as $attr)
                                                    <option
                                                        value="{{ $attr->name }}" {{ $attr->name ==  $attribute->name ? 'selected' : '' }}>{{ $attr->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="form-group">
                                            <label>مقدار ویژگی</label>
                                            <select name="attributes[{{ $loop->index }}][value]"
                                                    class="attribute-select form-control">
                                                <option value="">انتخاب کنید</option>
                                                @foreach($attribute->values as $value)
                                                    <option
                                                        value="{{ $value->value }}" {{ $value->id  === $attribute->pivot->value_id ? 'selected' : '' }}>{{ $value->value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <label>اقدامات</label>
                                        <div>
                                            <button type="button" class="btn btn-sm btn-warning"
                                                    onclick="document.getElementById('attribute-{{ $loop->index }}').remove()">
                                                حذف
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <hr>
                        <button class="btn btn-sm btn-danger" type="button" id="add_product_attribute">ویژگی جدید
                        </button>
                        <hr>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    ویرایش محصول
                                </button>
                                <a href="{{ route('admin.product.index') }}" class="btn btn-default mr-3">
                                    انصراف
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endcomponent
