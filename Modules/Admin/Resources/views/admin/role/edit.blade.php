@component('admin::admin.layout.content',['title'=>'ایجاد مقام'])

    @slot('breadcrumb')
        <li class="breadcrumb-item"><a href="/admin">پنل مقام ها</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.role.index') }}">مقام ها</a></li>
        <li class="breadcrumb-item active">ویرایش مقام</li>
    @endslot
    @slot('script')
        <script>
            $('#permission').select2({
                'placeholder':'دسترسی را انتخاب کنید'
            });
        </script>
    @endslot
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">ویرایش مقام</div>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $err)
                                    <li>
                                        {{ $err }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="{{route('admin.role.update',$role->id)}}" class="p-3">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">نام مقام :</label>

                            <div class="col-md-6">
                                <input id="name" type="text"
                                       class="form-control @error('name') is-invalid @enderror" name="name"
                                       required autocomplete="name" autofocus value="{{$role->name}}">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description"
                                   class="col-md-4 col-form-label text-md-right">توضیحات مقام : </label>

                            <div class="col-md-6">
                                <input id="description" type="text"
                                       class="form-control @error('email') is-invalid @enderror" name="description"
                                       value="{{ $role->description }}" required autocomplete="description">

                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="permission"
                                   class="col-md-4 col-form-label text-md-right">دسترسی ها(زیر مجموعه) : </label>
                            <select name="permission[]" id="permission" class="form-control" multiple>
                                @foreach(\App\Permission::all() as $permission)

                                    <option value="{{$permission->id}}" {{in_array($permission->id,$role->permissions->pluck('id')->toArray())?'selected':''}} >{{$permission->name}}</option>

                                @endforeach
                            </select>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    ویرایش مقام
                                </button>
                                <a href="{{ route('admin.role.index') }}" class="btn btn-default mr-3">
                                    انصراف
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endcomponent
