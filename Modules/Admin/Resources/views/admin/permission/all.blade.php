@component('admin::admin.layout.content',['title'=>'دسترسی ها'])

    @slot('breadcrumb')
        <li class="breadcrumb-item"><a href="/admin">پنل دسترسی ها</a></li>
        <li class="breadcrumb-item active">لیست دسترسی ها</li>
    @endslot
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">دسترسی ها</h3>

                    <div class="card-tools d-flex float-left">
                        <div class="input-group input-group-sm d-flex" style="width: 200px;">
                            <form action="" class="d-flex">
                                <input type="text" name="search" class="form-control float-right"
                                       placeholder="جستجو">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>
                        <div class="float-left  mr-2">
                            <a class="btn btn-sm btn-info" href="{{route('admin.permission.create')}}">ایجاد دسترسی</a>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>نام</th>
                            <th>توضیحات</th>
                            <th>عملیات</th>
                        </tr>
                        @foreach($permissions as $permission)
                            <tr>
                                <td>{{$permission->name}}</td>
                                <td>{{$permission->description}}</td>
                                <td class="d-flex btn-sm">
                                    <form action="{{route('admin.permission.destroy',$permission->id)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger ml-2">حذف</button>
                                    </form>
                                    <a href="{{ route('admin.permission.edit',$permission->id) }}"
                                       class="btn btn-sm btn-primary">ویرایش</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{$permissions->render()}}
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>


@endcomponent
