<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->name('admin.')->middleware('auth', 'admin.auth')->group(function () {
    Route::get('/', function () {
        return view('admin::admin.index');
    })->name('admin.');

    Route::resource('users', 'user\UsersController');

    Route::get('/{user}/permission', 'PermissionController@create')->name('user.permission')->middleware('can:staff-user-permission');
    Route::post('/{user}/permission', 'PermissionController@store')->name('user.permission.store')->middleware('can:staff-user-permission');

    Route::resource('permission', 'PermissionController');

    Route::resource('role', 'RoleController');

    Route::resource('product', 'ProductController');

    Route::get('comments/unapproved', 'CommentsController@unapproved')->name('comments.unapproved');
    Route::resource('comments', 'CommentsController');

    Route::resource('category', 'CategoryController');

    Route::post('/attribute/values', 'AttributeController@getValues');

    Route::resource('orders', 'OrderController');
    Route::get('order/{order}/payment', 'OrderController@payment')->name('order.payment');

    Route::resource('product.gallery', 'ProductGalleryController');

    Route::resource('discount', 'DiscountController');


    Route::get('module', 'MainController@index')->name('module.index');
    Route::put('module/{module}/disable', 'MainController@disable')->name('module.disable');
    Route::put('module/{module}/enable', 'MainController@enable')->name('module.enable');


});
