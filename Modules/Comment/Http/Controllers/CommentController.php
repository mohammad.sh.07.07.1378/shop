<?php

namespace Modules\Comment\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function comment(Request $request)
    {

//        if (!$request->ajax()) {
//            return response()->json([
//                'status' => 'error not request json'
//            ]);
//        }

        $data = $request->validate([
            'comment' => 'required',
            'commentable_id' => 'required',
            'commentable_type' => 'required',
            'parent_id' => 'required',
        ]);

        auth()->user()->comments()->create($data);

        alert()->success('ثبت شد');

        return back();

//
//        return response()->json([
//            'status' => 'success'
//        ]);

    }
}
