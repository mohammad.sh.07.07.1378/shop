<?php

namespace Modules\Cart\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cart\Helper\Cart;
use Modules\Product\Entities\Product;

class CartController extends Controller
{
    public function addToCart(Product $product)
    {
        if (Cart::count($product) < $product->inventory) {
            if (Cart::has($product)) {
                Cart::update($product, 1);
                alert()->success('یک مقدار دیگر از این محصول به سبد شما اضافه شد')->persistent('باشه');
            } else {
                Cart::put(
                    ['quantity' => 1],
                    $product
                );

                alert()->success('به سبد خرید شما اضافه شد')->persistent('باشه');

            }
        } else {

            alert()->error('کالا موجود نمیباشد')->persistent('باشه');
        }

        return back();
    }

    public function cart()
    {
        return view('cart::index');
    }

    public function quantityChange(Request $request)
    {

        $data = $request->validate([
            'quantity' => 'required',
            'id' => 'required',
        ]);

        if (Cart::has($data['id'])) {

            Cart::update($data['id'], [
                'quantity' => $data['quantity'],
            ]);

            return response([
                'msg' => 'success',
            ]);

        }

        return response([
            'msg' => 'error'
        ], 404);


    }

    public function delete($key)
    {

        Cart::delete($key);

        return back();
    }
}
