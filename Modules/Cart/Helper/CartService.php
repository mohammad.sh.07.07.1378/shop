<?php

namespace Modules\Cart\Helper;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;
use Modules\Discount\Entities\Discount;

class CartService
{
    protected $cart;

    public function __construct()
    {
        $cart = collect(json_decode(request()->cookie('cart'), true));
        $this->cart = $cart->count() ? $cart : collect([
            'item' => [],
            'discount' => null
        ]);
    }

    public function getDiscount()
    {
        return Discount::where('code', $this->cart['discount'])->first();
    }

    public function put(array $values, $obj = null)
    {
        if ($obj instanceof Model) {
            $values = array_merge($values, [
                'id' => Str::random(10),
                'subject_id' => $obj->id,
                'subject_type' => get_class($obj),
                'discount_percent' => 0
            ]);
        } elseif (!isset($values['id'])) {
            $values = array_merge($values, [
                'id' => Str::random(10)
            ]);
        }
        $this->cart['item'] = collect($this->cart['item'])->put($values['id'], $values)->toArray();

        Cookie::queue('cart', $this->cart, 24 * 3600);


        return $this;
    }

    public function has($key)
    {

        if ($key instanceof Model) {
            return !is_null(collect($this->cart['item'])->where('subject_id', $key->id)
                ->where('subject_type', get_class($key))->first());
        }

        return !is_null(collect($this->cart['item'])->where('id', $key)->first());
    }

    public function count($key)
    {

        if (!$this->has($key)) return 0;

        return $this->get($key)['quantity'];

    }

    public function get($key, $withRelationShipIfExists = true)
    {
        $item = $key instanceof Model
            ? collect($this->cart['item'])->where('subject_id', $key->id)->where('subject_type', get_class($key))->first()
            : collect($this->cart['item'])->where('id', $key)->first();

        return $withRelationShipIfExists
            ? $this->withRelationShipIfExists($item)
            : $item;

    }

    public function withRelationShipIfExists($item)
    {
        if (isset($item['subject_id']) && isset($item['subject_type'])) {
            $class = $item['subject_type'];
            $subject = (new $class())->find($item['subject_id']);
            $item[strtolower(class_basename($class))] = $subject;
            unset($item['subject_id']);
            unset($item['subject_type']);
            return $item;
        }
        return $item;
    }

    public function update($key, $quantity)
    {
        $item = collect($this->get($key, false));


        if (is_numeric($quantity)) {
            $item = $item->merge([
                'quantity' => $item['quantity'] + $quantity
            ]);
        }

        if (is_array($quantity)) {
            $item = $item->merge($quantity);
        }


        $this->put($item->toArray());

        return $this;

    }

    public function all()
    {
        $cart = $this->cart;
        $cart = collect($this->cart['item'])->map(function ($item) use ($cart) {
            $item = $this->withRelationShipIfExists($item);
            $item = $this->checkDiscountValidate($item, $cart['discount']);
            return $item;
        });
        return $cart;
    }

    public function addDiscount($code)
    {
        $this->cart['discount'] = $code;

        Cookie::queue('cart', $this->cart, 24 * 3600);
    }


    public function checkDiscountValidate($item, $discount)
    {
        $discount = Discount::where('code', $discount)->first();

        if ($discount && $discount->expired_at > now()) {
            if (
                (!$discount->products()->count() && !$discount->categories()->count()) ||
                in_array($item['product']->id, $discount->products()->plunk('id')->toArray()) ||
                array_intersect($item['product']->categories->plunk('id')->toArray(), $discount->categories()->plunk('id')->toArray())

            ) {
                $item['discount_percent'] = $discount->percent / 100;
            }
        }

        return $item;
    }

    public function delete($key)
    {

        if ($this->has($key)) {

            $this->cart = collect($this->cart['item'])->filter(function ($item) use ($key) {
                if ($key instanceof Model) {
                    return ($item['subject_id'] != $key['id']) && ($item['subject_type'] != get_class($key));
                }
                return $key != $item['id'];
            });

            Cookie::queue('cart', $this->cart, 24 * 3600);
            return true;

        }
        return false;
    }

}
